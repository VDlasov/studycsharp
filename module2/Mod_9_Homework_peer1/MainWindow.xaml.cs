﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Mod_9_Homework_peer1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        static List<Student> studentlist = new List<Student>();
        int studentcounter = 0;
        int counterposition = 0;

        private void btnCreateStudent_Click(object sender, RoutedEventArgs e)
        {
            var newstudent = new Student();
            newstudent.firstname = txtFirstName.Text;
            newstudent.lastname = txtLastName.Text;
            newstudent.city = txtCity.Text;

            studentlist.Add(newstudent);
            //MessageBox.Show("Student created");

            txtFirstName.Text = "";
            txtLastName.Text = "";
            txtCity.Text = "";
            studentcounter++;
        }

        private void btnPrevious_Click(object sender, RoutedEventArgs e)
        {
            if (studentcounter == 0)
            {
                MessageBox.Show("You must add a student first");
            }

            else
            {
                if (counterposition > 0)
                {
                    counterposition--;
                }
                else
                    counterposition = studentcounter - 1;
            }

            txtFirstName.Text = studentlist[counterposition].firstname;
            txtLastName.Text = studentlist[counterposition].lastname;
            txtCity.Text = studentlist[counterposition].city;
        }

        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            if (studentcounter == 0)
            {
                MessageBox.Show("You must add a student first");
            }

            else
            {
                if (counterposition < studentcounter - 1)
                {
                    counterposition++;
                }

                else
                    counterposition = 0;
            }

            txtFirstName.Text = studentlist[counterposition].firstname;
            txtLastName.Text = studentlist[counterposition].lastname;
            txtCity.Text = studentlist[counterposition].city;
        }

    }

    public class Student
    {
        public string firstname;
        public string lastname;
        public string city;

        #region Student properties
        public string FirstName
        {
            get
            {
                return firstname;
            }

            set
            {
                if (!String.IsNullOrWhiteSpace(value))
                    firstname = value;
                else
                    firstname = "Not provide";
            }
        }

        public string LastName
        {
            get
            {
                return lastname;
            }

            set
            {
                if (!String.IsNullOrWhiteSpace(value))
                    lastname = value;
                else
                    lastname = "Not provide";
            }
        }

        public string City
        {
            get
            {
                return city;
            }

            set
            {
                if (!String.IsNullOrWhiteSpace(value))
                    city = value;
                else
                    city = "Not provide";
            }
        }
        #endregion
    }
}