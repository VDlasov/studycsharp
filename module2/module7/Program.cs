﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace module7
{
    class Program
    {
        // Person base class with common attributes for a person
        abstract class Person
        {
            private string firstName;
            private string lastName;
            private DateTime birthday;
            private string address;

            #region property for Person private fileds
            public string FirstName
            {
                get
                {
                    return firstName;
                }

                set
                {
                    firstName = value;
                }
            }

            public string LastName
            {
                get
                {
                    return lastName;
                }

                set
                {
                    lastName = value;
                }
            }

            public DateTime Birthday
            {
                get
                {
                    return birthday;
                }

                set
                {
                    birthday = value;
                }
            }

            public string Address
            {
                get
                {
                    return address;
                }

                set
                {
                    address = value;
                }
            }
            #endregion
        }

        // Student class inherit from the Person base class
        class Student : Person
        {
            // 2 Added a Stack called Grades inside the Student object.
            Stack grades = new Stack();

            public Student()
            {
                Count++;
            }

            public static int Count { get; set; } = 0;


            #region properte for Student private fields
            public Stack Grades
            {
                get
                {
                    return grades;
                }

                set
                {
                    grades = value;
                }
            }
            #endregion


            public void TakeTest()
            {
                Console.WriteLine("{0} {1} is take the test", LastName, FirstName);
            }
        }

        // Teacher classe inherit from the Person base class 
        class Teacher : Person
        {
            public void GradeTest()
            {
                Console.WriteLine("{0} {1} is grade the test", LastName, FirstName);
            }
        }

        // class for a course
        class Course
        {
            string name;
            DateTime startDate;
            DateTime endDate;
            int numCreditHours;

            // 1 Used an ArrayList of Students, inside the Course object.
            ArrayList students = new ArrayList();
            Teacher[] teachers = new Teacher[3];

            public void ListStudents()
            {
                // 4 Used a foreach loop to output the first and last name of each Student in the ArrayList.
                // 5 Cast the object from the ArrayList to Student, inside the foreach loop, before printing out the name information.
                foreach (Student student in Students)
                {
                    Console.WriteLine("{0} {1}", student.FirstName, student.LastName);
                }
            }

            #region property for Course private fileds
            public string Name
            {
                get
                {
                    return name;
                }

                set
                {
                    name = value;
                }
            }

            public DateTime StartDate
            {
                get
                {
                    return startDate;
                }

                set
                {
                    startDate = value;
                }
            }

            public DateTime EndDate
            {
                get
                {
                    return endDate;
                }

                set
                {
                    endDate = value;
                }
            }

            public int NumCreditHours
            {
                get
                {
                    return numCreditHours;
                }

                set
                {
                    numCreditHours = value;
                }
            }

            public Teacher[] Teachers
            {
                get
                {
                    return teachers;
                }

                set
                {
                    teachers = value;
                }
            }

            public ArrayList Students
            {
                get
                {
                    return students;
                }

                set
                {
                    students = value;
                }
            }

            #endregion
        }

        // class for a degree
        class Degree
        {
            string type;
            string name;
            string description;
            Course course;

            #region property for Degree private fileds
            public string Type
            {
                get
                {
                    return type;
                }

                set
                {
                    type = value;
                }
            }

            public string Name
            {
                get
                {
                    return name;
                }

                set
                {
                    name = value;
                }
            }

            public string Description
            {
                get
                {
                    return description;
                }

                set
                {
                    description = value;
                }
            }

            public Course Course
            {
                get
                {
                    return course;
                }

                set
                {
                    course = value;
                }
            }
            #endregion
        }

        // class for a program
        class UProgram
        {
            string name;
            Degree degree;

            #region property for UProgram private fileds
            public string Name
            {
                get
                {
                    return name;
                }

                set
                {
                    name = value;
                }
            }

            public Degree Degree
            {
                get
                {
                    return degree;
                }

                set
                {
                    degree = value;
                }
            }
            #endregion
        }

        static void Main(string[] args)
        {
            // Instantiate three Student objects
            var student1 = new Student() { FirstName = "Denis", LastName = "Vlasov", Address = "Moscow", Birthday = new DateTime(1992, 02, 05) };
            var student2 = new Student() { FirstName = "Maksim", LastName = "Komrakov", Address = "Sydney", Birthday = new DateTime(1991, 07, 20) };
            var student3 = new Student() { FirstName = "Oleg", LastName = "Olegovich", Address = "New York", Birthday = new DateTime(1992, 09, 15) };

            student1.Grades.Push("5"); student1.Grades.Push("3"); student1.Grades.Push("4"); student1.Grades.Push("5"); student1.Grades.Push("3");
            student2.Grades.Push("4"); student2.Grades.Push("5"); student2.Grades.Push("4"); student2.Grades.Push("5"); student2.Grades.Push("3");
            student3.Grades.Push("3"); student3.Grades.Push("5"); student3.Grades.Push("4"); student3.Grades.Push("5"); student3.Grades.Push("3");

            // Instantiate at least one Teacher object
            var teacher1 = new Teacher() { FirstName = "Alesey", LastName = "Sukhanov", Address = "Italy", Birthday = new DateTime(1980, 05, 15) };

            // Instantiate a Course object called Programming with C#
            var course = new Course() { Name = "Programming with C#", StartDate = new DateTime(2015, 04, 07), EndDate = new DateTime(2015, 05, 17), NumCreditHours = 60 };

            // 3 Added 3 Student objects to this ArrayList using the ArrayList method for adding objects.
            course.Students.Add(student1);
            course.Students.Add(student2);
            course.Students.Add(student3);

            course.ListStudents();
            Console.ReadKey(true);
            return;
        }
    }
}
