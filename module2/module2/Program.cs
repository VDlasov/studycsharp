﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace module2
{
    class Program
    {
        static void Main(string[] args)
        {
            string x = "X";
            string o = "O";

            string res = "";

            //int n = 64;
            int n = 8;
            //int n = 1;

            for (int i = 1; i <= n; i++)
            {

                for (int j = 1; j <= n; j++)
                {


                    if ((i + j) % 2 == 0)
                    { // If eval go +"x", if odd +"o"
                        res += x;
                    }
                    else
                    {
                        res += o;
                    }
                }

                res += "\n";

            }

            Console.Write(res);
            Console.ReadKey();
        }
    }
}
