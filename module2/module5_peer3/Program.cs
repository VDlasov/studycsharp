﻿using System;

//For simplicity, I just write the properties that are used for this assignment.

namespace ModuleFiveAssignment
{
    public class Degree
    {
        public Course Courses { get; set; }

        public string Title { get; set; }
    }

    public class Course
    {
        public string Name { get; set; }

        public Student[] Students { get; set; }
        public Teacher[] Teachers { get; set; }

        public Course()
        {
            Students = new Student[3];
            Teachers = new Teacher[3];
        }
    }

    public class UProgram
    {
        public Degree Degrees { get; set; }

        public string Name { get; set; }

    }

    public class Teacher
    {
        public string FistName { get; set; }
        public string LastName { get; set; }
        public string Specialties { get; set; }

    }

    public class Student
    {
        public static int CountStudents { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Birthdate { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public int ZipPostal { get; set; }
        public string Country { get; set; }
        public string Comments { get; set; }
        public string Email { get; set; }

        public string GetName()
        {
            return String.Format("{0} {1}", this.FirstName, this.LastName);
        }

        public Student()
        {
            CountStudents++;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            //Creating students

            Student student1 = new Student();
            Student student2 = new Student();
            Student student3 = new Student();

            student1.FirstName = "John";
            student1.LastName = "Doe";

            student2.FirstName = "Sandy";
            student2.LastName = "Dee";

            student3.FirstName = "Peter";
            student3.LastName = "Parker";

            //Creating Course

            Course myCourse = new Course();
            myCourse.Name = "Programming with C#";

            //Assigning students to courses

            myCourse.Students[0] = student1;
            myCourse.Students[1] = student2;
            myCourse.Students[2] = student3;

            //Creating a Teacher

            Teacher myTeacher = new Teacher();
            myTeacher.FistName = "Charles";
            myTeacher.LastName = "Xavier";

            //Assigning teacher to course

            myCourse.Teachers[0] = myTeacher;

            //Creating a Degree

            Degree myDegree = new Degree();
            myDegree.Title = "Bachelor of Science";

            //Assigning Course to Degree

            myDegree.Courses = myCourse;

            //Creating a UProgram

            UProgram myUProgram = new UProgram();

            myUProgram.Name = "Information Technology";
            myUProgram.Degrees = myDegree;

            //Writing output

            Console.WriteLine("The {0} program contains the {1} degree.\n", myUProgram.Name, myDegree.Title);
            Console.WriteLine("The {0} degree contains the course {1}.\n", myDegree.Title, myDegree.Courses.Name);
            Console.WriteLine("The {0} course contains {1} students.\n", myDegree.Courses.Name, myCourse.Students.Length);

            Console.ReadKey();
        }
    }
}