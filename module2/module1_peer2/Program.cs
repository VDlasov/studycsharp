﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AssginmentModule1
{
    class Program
    {
        static void Main(string[] args)
        {
            string firstName = "Daniel";
            string lastName = "Andrews";
            DateTime bday = DateTime.Today;
            string addressLine1 = "3 Main Street";
            string addressLine2 = "Unit 202";
            string city = "Indianapolis";
            string State = "IN";
            string zip = "50607-2201";
            string country = "USA";
            string professor = "Prof. Alan Murray";
            string fieldofStudy = "Technology";
            string deptHead = "Jon Smith";
            string degree = "BA";

            Console.WriteLine(firstName);
            Console.WriteLine(lastName);
            Console.WriteLine(bday);
            Console.WriteLine(addressLine1);
            Console.WriteLine(addressLine2);
            Console.WriteLine(city);
            Console.WriteLine(State);
            Console.WriteLine(zip);
            Console.WriteLine(country);
            Console.WriteLine(professor);
            Console.WriteLine(fieldofStudy);
            Console.WriteLine(degree);
            Console.WriteLine(deptHead);

        }
    }
}