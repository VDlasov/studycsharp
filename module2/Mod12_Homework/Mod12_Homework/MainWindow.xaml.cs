﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using System.IO;

namespace Mod12_Homework
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private async void btnWriteFile_Click(object sender, RoutedEventArgs e)
        {
           await WriteFileAsync();
        }

        private async void btnReadFile_Click(object sender, RoutedEventArgs e)
        {
            await ReadFileAsync();
        }

        // 4 Modify the WriteFile() method to be asynchronous
        public async Task WriteFileAsync()
        {
            string filePath = @"SampleFile.txt";
            string text = txtContents.Text;

            await WriteTextAsync(filePath, text);
        }

        // 3.1-3.3 Modify the WriteText method
        // 8 Modify the call to ReadTextAsync appropriately for asynchronous operations
        private async Task WriteTextAsync(string filePath, string text)
        {
            byte[] encodedText = Encoding.Unicode.GetBytes(text);

            // 3.5 Modify the using (FileStream sourceStream.... )
            using (FileStream sourceStream = new FileStream(filePath,
                FileMode.Append, FileAccess.Write, FileShare.None,
                bufferSize: 4096, useAsync: true))
            {
                // 3.4 The FileStream class includes an asynchronous version of Write.
                await sourceStream.WriteAsync(encodedText, 0, encodedText.Length);
            };
        }

        // 7 Modify the ReadFile() method to be asynchronous with the correct method modifier
        public async Task ReadFileAsync()
        {
            string filePath = @"SampleFile.txt";

            if (File.Exists(filePath) == false)
            {
                MessageBox.Show(filePath + " not found", "File Error", MessageBoxButton.OK);
            }
            else
            {
                try
                {
                    string text = await ReadTextAsync(filePath);
                    txtContents.Text = text;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }
            }
        }

        // 6.1-6.3 Modify the ReadText method
        private async Task<string> ReadTextAsync(string filePath)
        {
            // 6.5 Modify the using (FileStream sourceStream.... ) in the ReadText method so that the sourceStream will use async
            using (FileStream sourceStream = new FileStream(filePath,
                FileMode.Open, FileAccess.Read, FileShare.Read,
                bufferSize: 4096, useAsync: true))
            {
                StringBuilder sb = new StringBuilder();

                byte[] buffer = new byte[0x1000];
                int numRead;
                // 6.4 Like the Write method, the Read method also comes in an asynchronous version
                while ((numRead = await sourceStream.ReadAsync(buffer, 0, buffer.Length)) != 0)
                {
                    string text = Encoding.Unicode.GetString(buffer, 0, numRead);
                    sb.Append(text);
                }

                return sb.ToString();
            }
        }
    }
}
