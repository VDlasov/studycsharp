﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Mod_9_Homework_peer2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Student> students = new List<Student>();
        int prev = 0, next = 0;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnCreateStudent_Click(object sender, RoutedEventArgs e)
        {
            Student student = new Student();
            student.Name = txtFirstName.Text;
            student.Surname = txtLastName.Text;
            student.City = txtCity.Text;
            student.Count++;
            students.Add(student);

            txtFirstName.Clear();
            txtLastName.Clear();
            txtCity.Clear();
        }

        private void btnPrevious_Click(object sender, RoutedEventArgs e)
        {
            if (prev == students.Count)
            {
                prev = 0;
                MessageBox.Show("Index out of range!");
            }

            int index = students.Count;
            txtFirstName.Text = students[index - 1 - prev].Name;
            txtLastName.Text = students[index - 1 - prev].Surname;
            txtCity.Text = students[index - 1 - prev].City;
            prev++;
        }

        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            if (next == students.Count)
            {
                next = 0;
                MessageBox.Show("Index out of range!");
            }

            txtFirstName.Text = students[next].Name;
            txtLastName.Text = students[next].Surname;
            txtCity.Text = students[next].City;
            next++;
        }
    }

    public class Student
    {
        public string firstname;
        public string lastname;
        public string city;
        public int count;

        #region Student properties
        public string Name
        {
            get
            {
                return firstname;
            }

            set
            {
                if (!String.IsNullOrWhiteSpace(value))
                    firstname = value;
                else
                    firstname = "Not provide";
            }
        }

        public string Surname
        {
            get
            {
                return lastname;
            }

            set
            {
                if (!String.IsNullOrWhiteSpace(value))
                    lastname = value;
                else
                    lastname = "Not provide";
            }
        }

        public string City
        {
            get
            {
                return city;
            }

            set
            {
                if (!String.IsNullOrWhiteSpace(value))
                    city = value;
                else
                    city = "Not provide";
            }
        }

        public int Count
        {
            get
            {
                return count;
            }

            set
            {
                count = value;
            }
        }
        #endregion
    }
}