﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace ConsoleApp7
{

    public class Person
    {

        private string firstName;
        public string FirstName { get { return firstName; } set { firstName = value; } }

        private string lastName;
        public string LastName { get { return lastName; } set { lastName = value; } }

        private string birthDate;
        public string BirthDate { get { return birthDate; } set { birthDate = value; } }

        private string homeAddress;
        public string HomeAddress { get { return homeAddress; } set { homeAddress = value; } }

        private int age;
        public Int32 Age { get { return age; } set { age = value; } }

        public Person(string fname, string lname, string bDate, string hAddress, int age)
        {

            FirstName = fname;
            LastName = lname;
            Age = age;
            BirthDate = bDate;
            HomeAddress = hAddress;

        }

        public void GradeTest() { }

    }
    public class Student : Person
    {

        private Int32 studentCounter = 0;

        public Int32 StudentCounter { get { return studentCounter; } set { studentCounter = value; } }

        public string studentID = null;

        public string StudentID { get { return studentID; } set { studentID = value; } }

        public Student(string fname, string lname, string bDate, string hAddress, int age, string studID)
        : base(fname, lname, bDate, hAddress, age)
        {
            StudentID = studID;

        }

        public void TakeTest() { }

        public void Grades(int grades)
        {

            Stack<int> grade = new Stack<int>();
            grade.Push(grades);

        }

    }

    public class Teacher : Person
    {

        private string teachersSubjectHandled = null;
        public string TeachersSubjectHandled { get { return teachersSubjectHandled; } set { teachersSubjectHandled = value; } }

        public Teacher(string fname, string lname, string hAddress, int age, string tshandled, string bDate)
        : base(fname, lname, bDate, hAddress, age)
        {
            FirstName = fname;
            LastName = lname;
            HomeAddress = hAddress;
            TeachersSubjectHandled = tshandled;
            Age = age;
            BirthDate = bDate;
        }
    }

    public class UProgram
    {
        private string[] programCategory = new string[100];
        public string[] ProgramCategory { get { return programCategory; } set { programCategory = value; } }

        private string courseDetails = null;
        public string CourseDetails { get { return courseDetails; } set { courseDetails = value; } }

        private string programFormat = null;
        public string ProgramFormat { get { return programFormat; } set { programFormat = value; } }

        private string subjectAreas = null;
        public string SubjectAreas { get { return subjectAreas; } set { subjectAreas = value; } }

        private string facultyBio = null;
        public string FacultyBio { get { return facultyBio; } set { facultyBio = value; } }

        public UProgram()
        {

        }
    }

    public class Course
    {

        private string[] courseInfo = new string[100];
        public string[] CourseInfo { get { return courseInfo; } set { courseInfo = value; } }

        private string[] courseInstructor = new string[100];
        public string[] CourseInstructor { get { return courseInstructor; } set { courseInstructor = value; } }

        private string courseLecture = null;
        public string CourseLecture { get { return courseLecture; } set { courseLecture = value; } }

        private string courseLocation = null;
        public string CourseLocation { get { return courseLocation; } set { courseLocation = value; } }

        private string addStudent;

        public string CourseStudent { get { return addStudent; } set { addStudent = value; } }

        public Int32 courseNumStudent = 0;

        public Int32 CourseNumStudent { get { return courseNumStudent; } set { courseNumStudent = value; } }

        public ArrayList newStudent;

        ArrayList addStud = new ArrayList();

        public Course() { }

        public void AddStudentToCourse(Object obj)
        {
            addStud.Add(obj);

        }

        public ArrayList ArrayStudent()
        {

            return addStud;

        }

        public void ListStudent()
        {
            int i = 0;
            foreach (Student a in ArrayStudent())
            {
                Console.WriteLine(i++ + ")" + a.FirstName + " " + a.LastName);
            }


        }

    }

    class Degree
    {

        private string[] degreeName = new string[100];

        public string[] DegreeName { get { return degreeName; } set { degreeName = value; } }

        public string[] degreeNameCourseTitle = new string[100];

        public string[] DegreeNameCourseTitle { get { return degreeNameCourseTitle; } set { degreeNameCourseTitle = value; } }

        public Degree() { }

    }

    class Program
    {
        static void Main(string[] args)
        {
            int numStudent = 0;

            //Create an array of students
            //Add 5 grades to each student

            Student newStud1 = new Student("John", "Doe", "September 14,1983", "Manila, Philippines", 31, "211775");
            newStud1.Grades(87);
            newStud1.Grades(88);
            newStud1.Grades(89);
            newStud1.Grades(84);
            newStud1.Grades(83);


            Student newStud2 = new Student("Shella", "Mae", "August 18,1995", "Manila, Philippines", 20, "211778");
            newStud1.Grades(90);
            newStud1.Grades(99);
            newStud1.Grades(87);
            newStud1.Grades(89);
            newStud1.Grades(87);

            Student newStud3 = new Student("Monroe", "Velozo", "August 18,1995", "Cotabato, Philippines", 20, "2118778");
            newStud1.Grades(87);
            newStud1.Grades(87);
            newStud1.Grades(87);
            newStud1.Grades(87);
            newStud1.Grades(87);

            //Create 3 student object and put it into an arraylist
            ArrayList arrayStudent = new ArrayList();
            arrayStudent.Add(newStud1);
            arrayStudent.Add(newStud2);
            arrayStudent.Add(newStud3);

            Teacher nTeacher = new Teacher("Steve", "John", "Apple Corporation", 31, "Programming with C#", "August 15, 1983");

            Course newCourse = new Course();
            newCourse.AddStudentToCourse(newStud1);
            newCourse.AddStudentToCourse(newStud2);
            newCourse.AddStudentToCourse(newStud3);


            Console.WriteLine("List of students from the course");
            newCourse.ListStudent();

            //The Number of student currently enrolled in the course
            newCourse.CourseNumStudent = arrayStudent.Count;
            numStudent = newCourse.CourseNumStudent; //keep track the number of student

            int c = 0;
            Console.WriteLine();
            Console.WriteLine("The number of student(s): {0}", numStudent);
            //List the student from the arrayStudent
            foreach (Student stud in arrayStudent)
            {
                Console.WriteLine(c++ + "){0} {1}", stud.FirstName, stud.LastName);

            }

            //Course Title
            newCourse.CourseInfo[0] = "Programming with C#";

            //Add the current teacher to course object
            newCourse.CourseInstructor[0] = nTeacher.FirstName + " " + nTeacher.LastName;

            //newCourse.CourseStudent[0] = newStud[0].FirstName + " " + newStud[0].LastName;
            //newCourse.CourseStudent[1] = newStud[1].FirstName + " " + newStud[1].LastName;
            //newCourse.CourseStudent[2] = newStud[2].FirstName + " " + newStud[2].LastName;

            Degree bachelor = new Degree();
            bachelor.DegreeName[0] = "Bachelor of Science";
            bachelor.DegreeName[1] = "Bachelor of Computer Engineering";

            UProgram newProgram = new UProgram();
            newProgram.ProgramCategory[0] = "Information Teachnology";
            newProgram.ProgramFormat = bachelor.DegreeName[0];

            //Console.WriteLine("The {0} program contains the {1} degree", newProgram.ProgramCategory[0], newProgram.ProgramFormat);
            //Console.WriteLine();
            //Console.WriteLine("The {0} degree contains the courses {1}", bachelor.DegreeName[0], newCourse.CourseInfo[0]);
            //Console.WriteLine();
            //Console.WriteLine("The {0} course contains {1} students(s)", newCourse.CourseInfo[0], newCourse.courseNumStudent);

        }
    }

}