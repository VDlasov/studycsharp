﻿using System;
using System.Collections.Generic;

class Program
{
    static void Main(string[] args)
    {
        //Studets, course and teacher:
        Student<int> student1 = new Student<int>();
        student1.Name = "Fernando";
        student1.LastName = "Concepción";
        student1.StudentCountry = "ES";
        student1.StudentZipPostal = 28000;
        student1.studentGrades.Push(90);
        student1.studentGrades.Push(80);
        student1.studentGrades.Push(91);
        student1.studentGrades.Push(92);
        student1.studentGrades.Push(33);

        Student<int> student2 = new Student<int>();
        student2.Name = "Juan";
        student2.LastName = "Jose";
        student2.StudentCountry = "ES";
        student2.StudentZipPostal = 46000;
        student2.studentGrades.Push(21);
        student2.studentGrades.Push(80);
        student2.studentGrades.Push(81);
        student2.studentGrades.Push(90);
        student2.studentGrades.Push(4);

        Student<int> student3 = new Student<int>();
        student3.Name = "Pepe";
        student3.LastName = "Jesus";
        student3.StudentCountry = "ES";
        student3.StudentZipPostal = 17000;
        student3.studentGrades.Push(1);
        student3.studentGrades.Push(11);
        student3.studentGrades.Push(30);
        student3.studentGrades.Push(2);
        student3.studentGrades.Push(100);

        Teacher teacher = new Teacher();
        teacher.Name = "Jonatan";
        teacher.LastName = "los santos";
        teacher.TeacherCountry = "ES";
        teacher.TeacherZipPostal = 90000;

        Course<Student<int>> curso = new Course<Student<int>>();
        curso.Information = "Programming with C#";

        curso.students.Add(student1);
        curso.students.Add(student2);
        curso.students.Add(student3);
        curso.teachers[0] = teacher;


        //Information call:
        curso.ListStudent();
        curso.ListTeacher();

        Console.WriteLine("Press any key to continue...");
        Console.ReadLine();

    }

    //class Persona:
    public class Person
    {
        private string name;
        private string last_name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string LastName
        {
            get { return last_name; }
            set { last_name = value; }
        }
    }

    //class Student:

    public class Student<T> : Person
    {
        private int studentzip_Postal;
        private string student_country;

        private Stack<T> grades = new Stack<T>();

        public string StudentCountry
        {
            get { return student_country; }
            set { student_country = value; }
        }

        public int StudentZipPostal
        {
            get { return studentzip_Postal; }
            set { studentzip_Postal = value; }
        }
        public Stack<T> studentGrades
        {
            get { return grades; }
            set { grades = value; }
        }

        public override string ToString()
        {
            string student_info = "Student: " + Name + " " + LastName + " Country: " + student_country + " . " + studentzip_Postal.ToString();
            student_info += "\n";
            student_info += "Grades: ";
            foreach (Object obj in grades)
            {
                student_info += obj.ToString() + ", ";
            }
            return student_info;
        }
    }

    //class Teacher

    public class Teacher : Person
    {
        private int teacher_ZipPostal;
        private string teacher_Country;

        public string TeacherCountry
        {
            get { return teacher_Country; }
            set { teacher_Country = value; }
        }

        public int TeacherZipPostal
        {
            get { return teacher_ZipPostal; }
            set { teacher_ZipPostal = value; }
        }

        public string TaskTest()
        {

            return "Teacher: " + Name + " " + LastName + " Country: " + teacher_Country + " . " + teacher_ZipPostal.ToString();
        }
    }

    //class Course
    public class Course<T>
    {

        public string Information { get; set; }

        public List<T> students = new List<T>();
        public Teacher[] teachers = new Teacher[3];

        public void ListStudent()
        {
            //Console
            Console.WriteLine("Course: {0}", Information);
            Console.WriteLine("This course contains {0} students:", students.Count);
            foreach (T student in students)
            {
                if (student != null)
                {
                    Console.WriteLine(student.ToString());
                }
            }
        }

        public void ListTeacher()
        {
            foreach (Teacher t in teachers)
            {
                if (t != null)
                {
                    Console.WriteLine(t.TaskTest());
                }
            }
        }
    }
}