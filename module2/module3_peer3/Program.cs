﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment3
{
    class Program
    {
        static void Main(string[] args)
        {
            GetStudentInfo();
            GetProfessorInfo();
            GetProgramInfo();
            GetDegreeInfo();
            GetCourseInfo();
        }

        static void GetStudentInfo()
        {
            Console.WriteLine("Enter the student's first name: ");
            string studentFirstName = Console.ReadLine();
            Console.WriteLine("Enter the student's last name: ");
            string studentLastName = Console.ReadLine();
            Console.WriteLine("Enter the student's address: ");
            string studentAddressLine1 = Console.ReadLine();
            Console.WriteLine("Enter the student's E-Mail: ");
            string studentEmail = Console.ReadLine();

            PrintStudentDetails(studentFirstName, studentLastName, studentAddressLine1, studentEmail);
        }

        static void GetProfessorInfo()
        {
            Console.WriteLine("Enter the professor's first name: ");
            string professorFirstName = Console.ReadLine();
            Console.WriteLine("Enter the professor's last name: ");
            string professorLastName = Console.ReadLine();
            Console.WriteLine("Enter the professor's E-Mail: ");
            string professorEmail = Console.ReadLine();

            PrintProfessorDetails(professorFirstName, professorLastName, professorEmail);
        }

        static void GetProgramInfo()
        {
            Console.WriteLine("Enter the college name: ");
            string collegeName = Console.ReadLine();
            Console.WriteLine("Enter the program name: ");
            string programName = Console.ReadLine();
            Console.WriteLine("Enter the department head: ");
            string departmentHead = Console.ReadLine();
            Console.WriteLine("Enter the fee: ");
            string programFee = Console.ReadLine();

            PrintProgramDetails(collegeName, programName, departmentHead, programFee);
        }

        static void GetDegreeInfo()
        {
            Console.WriteLine("Enter the degree type: ");
            string degreeType = Console.ReadLine();
            Console.WriteLine("Enter the degree name: ");
            string degreeName = Console.ReadLine();
            Console.WriteLine("Enter the degree description: ");
            string degreeDescription = Console.ReadLine();

            PrintDegreeDetails(degreeType, degreeName, degreeDescription);
        }

        static void GetCourseInfo()
        {
            Console.WriteLine("Enter the course code: ");
            string courseCode = Console.ReadLine();
            Console.WriteLine("Enter the course name: ");
            string courseName = Console.ReadLine();
            Console.WriteLine("Enter the course description: ");
            string courseDescription = Console.ReadLine();
            Console.WriteLine("Enter the course credit: ");
            string courseCredits = Console.ReadLine();

            PrintCourseDetails(courseCode, courseName, courseDescription, courseCredits);
        }

        /* Print Details */
        static void PrintStudentDetails(string pFirstName, string pLastName, string pAddress, string pEmail)
        {
            Console.WriteLine("----------------------------------------");
            Console.WriteLine("Student Information");
            Console.WriteLine("----------------------------------------");
            Console.WriteLine(" First Name: " + pFirstName);
            Console.WriteLine(" Last Name: " + pLastName);
            Console.WriteLine(" Address : " + pAddress);
            Console.WriteLine(" E-mail: " + pEmail);
            Console.WriteLine("----------------------------------------");
            Console.WriteLine("Press any key to continue");
            Console.ReadKey();
            Console.Clear();
        }

        static void PrintProfessorDetails(string pFirstName, string pLastName, string pEmail)
        {
            Console.WriteLine("Professor Information");
            Console.WriteLine("----------------------------------------");
            Console.WriteLine(" First Name: " + pFirstName);
            Console.WriteLine(" Last Name: " + pLastName);
            Console.WriteLine(" E-mail: " + pEmail);
            Console.WriteLine("----------------------------------------");
            Console.WriteLine("Press any key to continue");
            Console.ReadKey();
            Console.Clear();
        }

        static void PrintProgramDetails(string pCollege, string pProgram, string pHead, string pFee)
        {
            Console.WriteLine("Program Information");
            Console.WriteLine("----------------------------------------");
            Console.WriteLine(" College: " + pCollege);
            Console.WriteLine(" Name: " + pProgram);
            Console.WriteLine(" Dep. Head: " + pHead);
            Console.WriteLine(" Fee: US$ " + pFee);
            Console.WriteLine("----------------------------------------");
            Console.WriteLine("Press any key to continue");
            Console.ReadKey();
            Console.Clear();
        }

        static void PrintDegreeDetails(string pType, string pName, string pDescription)
        {
            Console.WriteLine("Degree Information");
            Console.WriteLine("----------------------------------------");
            Console.WriteLine(" Type: " + pType);
            Console.WriteLine(" Name: " + pName);
            Console.WriteLine(" Description: " + pDescription);
            Console.WriteLine("----------------------------------------");
            Console.WriteLine("Press any key to continue");
            Console.ReadKey();
            Console.Clear();
        }

        static void PrintCourseDetails(string pCode, string pName, string pDescription, string pCredit)
        {
            Console.WriteLine("Course Information");
            Console.WriteLine("----------------------------------------");
            Console.WriteLine(" Code: " + pCode);
            Console.WriteLine(" Name: " + pName);
            Console.WriteLine(" Description: " + pDescription);
            Console.WriteLine(" Credits: " + pCredit);
            Console.WriteLine("----------------------------------------");
            Console.WriteLine("Press any key to terminate program");
            Console.ReadKey();
        }
    }
}