﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace module6
{
    class Program
    {
        // 1 Create a Person base class with common attributes for a person
        abstract class Person
        {
            private string firstName;
            private string lastName;
            private DateTime birthday;
            private string address;

            #region property for Person private fileds
            public string FirstName
            {
                get
                {
                    return firstName;
                }

                set
                {
                    firstName = value;
                }
            }

            public string LastName
            {
                get
                {
                    return lastName;
                }

                set
                {
                    lastName = value;
                }
            }

            public DateTime Birthday
            {
                get
                {
                    return birthday;
                }

                set
                {
                    birthday = value;
                }
            }

            public string Address
            {
                get
                {
                    return address;
                }

                set
                {
                    address = value;
                }
            }
            #endregion
        }

        // 2 Make your Student and Teacher classes inherit from the Person base class
        // 3 Modify your Student and Teacher classes so that they inherit the common attributes from Person
        // 4 Modify your Student and Teacher classes so they include characteristics specific to their type. 
        class Student : Person
        {
            public Student()
            {
                Count++;
            }

            public static int Count { get; set; } = 0;

            public void TakeTest()
            {
                Console.WriteLine("{0} {1} is take the test", LastName, FirstName);
            }
        }

        // 2 Make your Student and Teacher classes inherit from the Person base class
        // 3 Modify your Student and Teacher classes so that they inherit the common attributes from Person
        // 4 Modify your Student and Teacher classes so they include characteristics specific to their type. 
        class Teacher : Person
        {
            public void GradeTest()
            {
                Console.WriteLine("{0} {1} is grade the test", LastName, FirstName);
            }
        }

        // class for a course
        class Course
        {
            string name;
            DateTime startDate;
            DateTime endDate;
            int numCreditHours;
            Student[] students = new Student[3];
            Teacher[] teachers = new Teacher[3];

            #region property for Course private fileds
            public string Name
            {
                get
                {
                    return name;
                }

                set
                {
                    name = value;
                }
            }

            public DateTime StartDate
            {
                get
                {
                    return startDate;
                }

                set
                {
                    startDate = value;
                }
            }

            public DateTime EndDate
            {
                get
                {
                    return endDate;
                }

                set
                {
                    endDate = value;
                }
            }

            public int NumCreditHours
            {
                get
                {
                    return numCreditHours;
                }

                set
                {
                    numCreditHours = value;
                }
            }

            public Student[] Students
            {
                get
                {
                    return students;
                }

                set
                {
                    students = value;
                }
            }

            public Teacher[] Teachers
            {
                get
                {
                    return teachers;
                }

                set
                {
                    teachers = value;
                }
            }

            #endregion
        }

        // class for a degree
        class Degree
        {
            string type;
            string name;
            string description;
            Course course;

            #region property for Degree private fileds
            public string Type
            {
                get
                {
                    return type;
                }

                set
                {
                    type = value;
                }
            }

            public string Name
            {
                get
                {
                    return name;
                }

                set
                {
                    name = value;
                }
            }

            public string Description
            {
                get
                {
                    return description;
                }

                set
                {
                    description = value;
                }
            }

            public Course Course
            {
                get
                {
                    return course;
                }

                set
                {
                    course = value;
                }
            }
            #endregion
        }

        // class for a program
        class UProgram
        {
            string name;
            Degree degree;

            #region property for UProgram private fileds
            public string Name
            {
                get
                {
                    return name;
                }

                set
                {
                    name = value;
                }
            }

            public Degree Degree
            {
                get
                {
                    return degree;
                }

                set
                {
                    degree = value;
                }
            }
            #endregion
        }

        // Run the same code in Program.cs from Module 5
        static void Main(string[] args)
        {
            // Instantiate three Student objects
            var student1 = new Student() { FirstName = "Denis", LastName = "Vlasov", Address = "Moscow", Birthday = new DateTime(1992, 02, 05) };
            var student2 = new Student() { FirstName = "Maksim", LastName = "Komrakov", Address = "Sydney", Birthday = new DateTime(1991, 07, 20) };
            var student3 = new Student() { FirstName = "Oleg", LastName = "Olegovich", Address = "New York", Birthday = new DateTime(1992, 09, 15) };

            // Instantiate at least one Teacher object
            var teacher1 = new Teacher() { FirstName = "Alesey", LastName = "Sukhanov", Address = "Italy", Birthday = new DateTime(1980, 05, 15) };

            // Instantiate a Course object called Programming with C#
            var course = new Course() { Name = "Programming with C#", StartDate = new DateTime(2015, 04, 07), EndDate = new DateTime(2015, 05, 17), NumCreditHours = 60 };

            // Add your three students to this Course object
            course.Students[0] = student1;
            course.Students[1] = student2;
            course.Students[2] = student3;

            // Add that Teacher object to your Course object
            course.Teachers[0] = teacher1;

            // Instantiate a Degree object, such as Bachelor
            var degree = new Degree() { Type = "Bachelor of Science", Description = "Mathematics and Informatics", Name = "Applied physics and mathematics" };

            // Add your Course object to the Degree object.
            degree.Course = course;

            // Instantiate a UProgram object called Information Technology
            var uProgram = new UProgram() { Name = "Information Technology" };

            // Add the Degree object to the UProgram object
            uProgram.Degree = degree;

            // Using Console.WriteLine statements, output the following information to the console window
            Console.WriteLine("The {0} programm contains the {1} degree", uProgram.Name, uProgram.Degree.Type);
            Console.WriteLine("The {0} degree contains the course {1}", uProgram.Degree.Type, uProgram.Degree.Course.Name);
            Console.WriteLine("The {0} course contains {1} student<s>", uProgram.Degree.Course.Name, Student.Count);
            Console.ReadKey(true);

        }
    }
}
