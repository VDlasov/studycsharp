﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace module4
{
    class ProgramModule4
    {
        // Create a struct to represent a student
        struct Student
        {
            public string firstName;
            public string lastName;
            public DateTime birthday;
            public string address;

            public Student(string firstName, string lastName, DateTime birthday, string address)
            {
                this.firstName = firstName;
                this.lastName = lastName;
                this.birthday = birthday;
                this.address = address;
            }
        }

        // Create a struct to represent a teacher
        struct Teacher
        {
            public string firstName;
            public string lastName;
            public DateTime birthday;
            public string address;

            public Teacher(string firstName, string lastName, DateTime birthday, string address)
            {
                this.firstName = firstName;
                this.lastName = lastName;
                this.birthday = birthday;
                this.address = address;
            }
        }

        // Create a struct to represent a course
        struct Course
        {
            public string name;
            public DateTime startDate;
            public DateTime endDate;
            public int numCreditHours;

            public Course(string name, DateTime startDate, DateTime endDate, int numCreditHours)
            {
                this.name = name;
                this.startDate = startDate;
                this.endDate = endDate;
                this.numCreditHours = numCreditHours;
            }
        }

        // Create a struct to represent a program
        struct Program
        {
            public string subjects;
            public string exams;
            public string coursework;

            public Program(string subjects, string exams, string coursework)
            {
                this.subjects = subjects;
                this.exams = exams;
                this.coursework = coursework;
            }
        }

        static void Main(string[] args)
        {
            // Create an array to hold 5 student structs
            Student[] students = new Student[5];

            // Assign values to the fields in at least one of the student structs in the array
            for (int i = 0; i < students.Length; i++)
            {
                students[i].firstName = string.Format("Denis{0}", i);
                students[i].lastName = string.Format("Vlasov{0}", i);
                students[i].address = string.Format("Moscow{0}", i);
                students[i].birthday = new DateTime(1992, 02, i+1);
            }

            // Using a series of Console.WriteLine() statements, output the values for the student struct that you assigned in the previous step
            for (int i = 0; i < students.Length; i++)
            {
                Console.WriteLine(students[i].firstName);
                Console.WriteLine(students[i].lastName);
                Console.WriteLine(students[i].address);
                Console.WriteLine(students[i].birthday.ToShortDateString());
                Console.WriteLine();
            }


        }

    }
}
