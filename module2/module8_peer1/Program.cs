﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    //Class base
    class Person
    {
        //encapsulate of member variables
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }

        //constructor
        public Person(string firstName, string lastName, int age)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Age = age;
        }
    }

    //Class Student
    class Student : Person
    {
        //encapsulate of member variables
        public int CountDisciplineCourse { get; set; }
        public Stack<int> Grades { get; set; }

        //static variable
        public static int CountStudents;

        //constructor with properties of class inherited 
        public Student(string firstName, string lastName, int age, int countDisciplineCourse)
        : base(firstName, lastName, age)
        {
            //prop this class
            this.CountDisciplineCourse = countDisciplineCourse;

            CountStudents++;

            //initializes a new Stack list
            Grades = new Stack<int>();
        }

        //Method to Add 5 grades to the the Stack<T> in the Student object
        public void StoreTestScore(int value)
        {
            Grades.Push(value);
        }

    }

    //Class Teacher
    class Teacher : Person
    {
        //encapsulate of member variables
        public int CountDisciplineTeach { get; set; }

        //constructor with properties of class inherited 
        public Teacher(string firstName, string lastName, int age, int countDisciplineTeach)
        : base(firstName, lastName, age)
        {
            //prop this class
            this.CountDisciplineTeach = countDisciplineTeach;
        }

        //Method specific
        public void CreateTest() { }
    }

    //Class Course
    class Course
    {
        //encapsulate of member variables
        public string CourseName { get; set; }
        public List<Student> ListStudent { get; set; }
        public List<Teacher> ListTeacher { get; set; }

        //constructor
        public Course(string courseName)
        {
            this.CourseName = courseName;

            //inicialize student's generic list
            this.ListStudent = new List<Student>();

            //inicialize teacher's generic list
            this.ListTeacher = new List<Teacher>();
        }

        //Method to do a foreach loop, iterate over the Students in the List<T>
        public void ListStudents()
        {
            foreach (Student student in ListStudent)
            {
                Console.WriteLine(" Student's name: {0} {1}", student.FirstName, student.LastName);
            }
        }
    }

    //Class Degree
    class Degree
    {
        //encapsulate of member variables
        public string DegreeName { get; set; }
        public string DegreeType { get; set; }
        public Course VarCourse { get; set; }

        //constructor
        public Degree(string degreeName, string degreeType)
        {
            this.DegreeName = degreeName;
            this.DegreeType = degreeType;
        }
    }

    //Class UProgram
    class UProgram
    {
        public string NameProgram { get; set; }
        public Degree VarDegree { get; set; }

        public UProgram(string nameProgram)
        {
            this.NameProgram = nameProgram;
        }
    }

    //Class Main
    class ClassProgram
    {
        static void Main(string[] args)
        {
            //NOTE: I prefer declarate all the variables before use

            //Instantiate three Student objects with inherited
            Student student1 = new Student("Chris", "Oliver", 46, 3);
            Student student2 = new Student("Sarah", "Ferraz", 22, 5);
            Student student3 = new Student("Roberto", "Swu", 32, 4);

            //Instantiate at least one Teacher object with inherited
            Teacher teacher1 = new Teacher("Gabriel", "Mosby", 32, 2);

            //Instantiate a Course object called Programming with C#
            Course course = new Course("Programing with C#");

            //Instantiate a Degree object, such as Bachelor
            Degree degree = new Degree("Science Computer", "Bachelor");

            //Instantiate a UProgram object called Information Technology.
            UProgram uProgram = new UProgram("Information Technology");

            //Add 5 grades to the the Stack in the each Student object 
            //to student1
            for (int i = 2; i < 7; i++) { student1.StoreTestScore(i); }
            //to student2
            for (int i = 4; i < 10; i++) { student2.StoreTestScore(i); }
            //to student3
            for (int i = 1; i < 6; i++) { student3.StoreTestScore(i); }

            //Add three students to 'course' object in generic list
            course.ListStudent.Add(student1);
            course.ListStudent.Add(student2);
            course.ListStudent.Add(student3);

            //Add Teacher object to your Course object in generic list
            course.ListTeacher.Add(teacher1);

            //Add your 'course' object to the Degree object
            degree.VarCourse = course;

            //Add the 'degree' object to the UProgram object.
            uProgram.VarDegree = degree;

            //Print informations
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("\n *");
            Console.ForegroundColor = ConsoleColor.Gray;

            Console.Write(" Module 8 - Generic List\n\n");

            course.ListStudents();

            Console.ReadKey();
        }
    }
}