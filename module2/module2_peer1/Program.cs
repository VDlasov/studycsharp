﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace module2_peer1
{
    class Program
    {
        static void Main(string[] args)
        {
            Boolean xWrite = true;
            int row, line;

            for (line = 1; line < 9; line++)
            {
                for (row = 1; row < 9; row++)
                {
                    Console.Write(xWrite ? "X" : "O");
                    xWrite = !xWrite;
                }
                Console.WriteLine("");
                // change again for the starting char on next line
                xWrite = !xWrite;
            }

            // wait for a keypress
            Console.ReadKey();
        }
    }
}
