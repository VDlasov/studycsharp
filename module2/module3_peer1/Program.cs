﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyModule3
{
    class Program
    {
        static void GetStudentInfo()
        {
            Console.WriteLine("Enter the student's first name: ");
            string firstName = Console.ReadLine();
            Console.WriteLine("Enter the student's last name: ");
            string lastName = Console.ReadLine();
            Console.WriteLine("Enter the student's birthday: ");
            string birthday = Console.ReadLine();

            PrintStudentDetails(firstName, lastName, birthday);
        }

        static void PrintStudentDetails(string first, string last, string birthday)
        {
            Console.WriteLine("{0} {1} was born on: {2}", first, last, birthday);
        }

        static void GetTeacherInfo()
        {
            Console.WriteLine("Enter the teacher's title: ");
            string title = Console.ReadLine();
            Console.WriteLine("Enter the teacher's first name: ");
            string firstName = Console.ReadLine();
            Console.WriteLine("Enter the teacher's last name: ");
            string lastName = Console.ReadLine();

            PrintTeacherDetails(title, firstName, lastName);
        }

        static void PrintTeacherDetails(string title, string first, string last)
        {
            Console.WriteLine("Teaher: {2} {0} {1}", first, last, title);
        }

        static void GetCourseInfo()
        {
            Console.WriteLine("Enter the course's name: ");
            string name = Console.ReadLine();
            Console.WriteLine("Enter the course's hours(int): ");
            int hours = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter the course's credits(int): ");
            int credits = Convert.ToInt32(Console.ReadLine());

            PrintCourseDetails(name, hours, credits);
        }

        static void PrintCourseDetails(string name, int hours, int credits)
        {
            Console.WriteLine("The course {0} takes {1} hours and gives {2} credits", name, hours, credits);
        }

        static void GetProgramInfo()
        {
            Console.WriteLine("Enter the program's name: ");
            string name = Console.ReadLine();
            Console.WriteLine("Enter the program's offered degree: ");
            string degree = Console.ReadLine();
            Console.WriteLine("Enter the program's department head: ");
            string depHead = Console.ReadLine();

            PrintProgramDetails(name, degree, depHead);
        }

        static void PrintProgramDetails(string name, string degree, string dHead)
        {
            Console.WriteLine("The program {0} offers {1} degree and the head of the department is {2}", name, degree, dHead);
        }

        static void GetDegreeInfo()
        {
            Console.WriteLine("Enter the degree's type: ");
            string degree = Console.ReadLine();
            Console.WriteLine("Enter the degree's name: ");
            string name = Console.ReadLine();
            Console.WriteLine("Enter the degree's required credits(int): ");
            int credits = Convert.ToInt32(Console.ReadLine());

            PrintDegreeDetails(degree, name, credits);
        }

        static void PrintDegreeDetails(string degree, string name, int credits)
        {
            Console.WriteLine("The {0} degree for {1} requires {2} credits", degree, name, credits);
        }

        static void ValidateBirthday(string bd)
        {
            throw new NotImplementedException();
        }

        static void Main(string[] args)
        {
            GetStudentInfo();
            GetTeacherInfo();
            GetCourseInfo();
            GetProgramInfo();
            GetDegreeInfo();

            try
            {
                ValidateBirthday("02/29/2000");
            }
            catch (Exception ex)
            {
                Console.WriteLine("An exception was thrown with the following message: {0}", ex.Message);
            }

            // stuff to make program wait before exiting
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}