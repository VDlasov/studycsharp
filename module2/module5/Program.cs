﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace module5
{
    class Program
    {
        // Create a class file for a student
        class Student
        {
            private string firstName;
            private string lastName;
            private DateTime birthday;
            private string address;

            private static int count = 0;

            public Student()
            {
                count++;
            }

            #region property for Student private fileds
            public string FirstName
            {
                get
                {
                    return firstName;
                }

                set
                {
                    firstName = value;
                }
            }

            public string LastName
            {
                get
                {
                    return lastName;
                }

                set
                {
                    lastName = value;
                }
            }

            public DateTime Birthday
            {
                get
                {
                    return birthday;
                }

                set
                {
                    birthday = value;
                }
            }

            public string Address
            {
                get
                {
                    return address;
                }

                set
                {
                    address = value;
                }
            }

            public static int Count
            {
                get
                {
                    return count;
                }

                set
                {
                    count = value;
                }
            }
            #endregion
        }

        // Create a class file for a teacher
        class Teacher
        {
            private string firstName;
            private string lastName;
            private DateTime birthday;
            private string address;

            #region property for Teacher private fileds
            public string FirstName
            {
                get
                {
                    return firstName;
                }

                set
                {
                    firstName = value;
                }
            }

            public string LastName
            {
                get
                {
                    return lastName;
                }

                set
                {
                    lastName = value;
                }
            }

            public DateTime Birthday
            {
                get
                {
                    return birthday;
                }

                set
                {
                    birthday = value;
                }
            }

            public string Address
            {
                get
                {
                    return address;
                }

                set
                {
                    address = value;
                }
            }
            #endregion
        }

        // Create a class file for a course
        class Course
        {
            string name;
            DateTime startDate;
            DateTime endDate;
            int numCreditHours;
            Student[] students = new Student[3];
            Teacher[] teachers = new Teacher[3];

            #region property for Course private fileds
            public string Name
            {
                get
                {
                    return name;
                }

                set
                {
                    name = value;
                }
            }

            public DateTime StartDate
            {
                get
                {
                    return startDate;
                }

                set
                {
                    startDate = value;
                }
            }

            public DateTime EndDate
            {
                get
                {
                    return endDate;
                }

                set
                {
                    endDate = value;
                }
            }

            public int NumCreditHours
            {
                get
                {
                    return numCreditHours;
                }

                set
                {
                    numCreditHours = value;
                }
            }

            public Student[] Students
            {
                get
                {
                    return students;
                }

                set
                {
                    students = value;
                }
            }

            public Teacher[] Teachers
            {
                get
                {
                    return teachers;
                }

                set
                {
                    teachers = value;
                }
            }

            #endregion
        }

        // Create a class file for a degree
        class Degree
        {
            string type;
            string name;
            string description;
            Course course;

            #region property for Degree private fileds
            public string Type
            {
                get
                {
                    return type;
                }

                set
                {
                    type = value;
                }
            }

            public string Name
            {
                get
                {
                    return name;
                }

                set
                {
                    name = value;
                }
            }

            public string Description
            {
                get
                {
                    return description;
                }

                set
                {
                    description = value;
                }
            }

            public Course Course
            {
                get
                {
                    return course;
                }

                set
                {
                    course = value;
                }
            }
            #endregion
        }

        // Create a class file for a program
        class UProgram
        {
            string name;
            Degree degree;

            #region property for UProgram private fileds
            public string Name
            {
                get
                {
                    return name;
                }

                set
                {
                    name = value;
                }
            }

            public Degree Degree
            {
                get
                {
                    return degree;
                }

                set
                {
                    degree = value;
                }
            }
            #endregion
        }



        static void Main(string[] args)
        {
            // 1 Instantiate three Student objects
            var student1 = new Student() { FirstName = "Denis", LastName = "Vlasov", Address = "Moscow", Birthday = new DateTime(1992, 02, 05) };
            var student2 = new Student() { FirstName = "Maksim", LastName = "Komrakov", Address = "Sydney", Birthday = new DateTime(1991, 07, 20) };
            var student3 = new Student() { FirstName = "Oleg", LastName = "Olegovich", Address = "New York", Birthday = new DateTime(1992, 09, 15) };

            // 4 Instantiate at least one Teacher object
            var teacher1 = new Teacher() { FirstName = "Alesey", LastName = "Sukhanov", Address = "Italy", Birthday = new DateTime(1980, 05, 15) }; 

            // 2 Instantiate a Course object called Programming with C#
            var course = new Course() { Name = "Programming with C#", StartDate = new DateTime(2015, 04, 07), EndDate = new DateTime(2015, 05, 17), NumCreditHours = 60 };

            // 3 Add your three students to this Course object
            course.Students[0] = student1;
            course.Students[1] = student2;
            course.Students[2] = student3;

            //5  Add that Teacher object to your Course object
            course.Teachers[0] = teacher1;

            // 6 Instantiate a Degree object, such as Bachelor
            var degree = new Degree() { Type = "Bachelor of Science", Description = "Mathematics and Informatics", Name = "Applied physics and mathematics" };

            // 7 Add your Course object to the Degree object.
            degree.Course = course;

            // 8 Instantiate a UProgram object called Information Technology
            var uProgram = new UProgram() { Name = "Information Technology" };

            // 9 Add the Degree object to the UProgram object
            uProgram.Degree = degree;

            // 10 Using Console.WriteLine statements, output the following information to the console window
            Console.WriteLine("The {0} programm contains the {1} degree", uProgram.Name, uProgram.Degree.Type);
            Console.WriteLine("The {0} degree contains the course {1}", uProgram.Degree.Type, uProgram.Degree.Course.Name);
            Console.WriteLine("The {0} course contains {1} student<s>", uProgram.Degree.Course.Name, Student.Count);
            Console.ReadKey(true);
        }
    }
}
