﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_4
{
    class Program
    {
        struct student
        {
            public student(string fname, string lname, int rno)
            {
                this.fname = fname;
                this.lname = lname;
                this.rno = rno;
            }
            public string fname;
            public string lname;
            public int rno;
        }

        struct teacher
        {
            public teacher(string fname, string lname, int id)
            {
                this.fname = fname;
                this.lname = lname;
                this.id = id;
            }
            string fname;
            string lname;
            int id;

        }

        struct course
        {
            public course(string name, string code, string instructor)
            {
                this.name = name;
                this.code = code;
                this.instructor = instructor;
            }
            string name;
            string code;
            string instructor;

        }

        struct program
        {
            public program(string name, string id, string duration)
            {
                this.name = name;
                this.id = id;
                this.duration = duration;
            }
            string name;
            string id;
            string duration;
        }
        static void Main(string[] args)
        {
            student[] s = new student[5];
            course c = new course("Computer Architecture", "CS202", "Dr.V.K.Jain");
            teacher t = new teacher("Vinod", "Jain", 10);
            program p = new program("Undergraduate", "2", "4");
            s[0].fname = "Devansh";
            s[0].lname = "Tiwari";
            s[0].rno = 64;
            Console.WriteLine("Name of the student: " + s[0].fname + " " + s[0].lname + "\nRoll Number: " + s[0].rno);
            Console.ReadKey(true);
        }
    }
}