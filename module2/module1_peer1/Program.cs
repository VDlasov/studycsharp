﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {

            string stFirstName;
            string stLastName;
            DateTime stBirthDay;
            string stAddressLine1;
            string stAddressLine2;
            string stCity;
            string stStateProvince;
            int stZipPostal;
            string stCountry;

            string prFirstName;
            string prLastName;
            DateTime prBirthDay;
            string prAddressLine1;
            string prAddressLine2;
            string prCity;
            string prStateProvince;
            int prZipPostal;
            string prCountry;

            string universityDegree;

            string upProgramName;
            string upDegreeOffered;
            string upDepartmendHead;

            stFirstName = "Alex";
            stLastName = "Belletti";
            stBirthDay = new DateTime(1986, 11, 26);
            stAddressLine1 = "Via Berlino 40";
            stAddressLine2 = "";
            stCity = "Aprilia";
            stStateProvince = "Lazio";
            stZipPostal = 04011;
            stCountry = "Italy";

            prFirstName = "Lallo";
            prLastName = "Lello";
            prBirthDay = new DateTime(2000, 1, 1);
            prAddressLine1 = "Via Rome";
            prAddressLine2 = "Via CaputMundi";
            prCity = "Latina";
            prStateProvince = "Campania";
            prZipPostal = 00186;
            prCountry = "Italy";

            universityDegree = "Science in Information Technology";

            upProgramName = "Agriculture, Food, Forestry, Resource Management, Vet Sciences";
            upDegreeOffered = "Bachelor, Master, Ph.D.";
            upDepartmendHead = "Lallo,Lello";

            Console.WriteLine("Student name: " + stFirstName);
            Console.WriteLine("Student Lastname: " + stLastName);
            Console.WriteLine("Student birthday: " + stBirthDay);
            Console.WriteLine("Student Address: " + stAddressLine1);
            Console.WriteLine("student Address2: " + stAddressLine2);
            Console.WriteLine("Stident City: " + stCity);
            Console.WriteLine("Student State: " + stStateProvince);
            Console.WriteLine("Student zipCode: " + stZipPostal);
            Console.WriteLine("Student Country: " + stCountry + "\r\n");

            Console.WriteLine("Professor FirstName: " + prFirstName);
            Console.WriteLine("Professor lastname: " + prLastName);
            Console.WriteLine("Professor Birthday: " + prBirthDay);
            Console.WriteLine("Professor Address: " + prAddressLine1);
            Console.WriteLine("Professor Address2: " + prAddressLine2);
            Console.WriteLine("Professor City: " + prCity);
            Console.WriteLine("Professor State/Province: " + prStateProvince);
            Console.WriteLine("Professor ZipCode: " + prZipPostal);
            Console.WriteLine("Professor Country: " + prCountry + "\r\n");
            Console.WriteLine("University Degree: " + universityDegree);
            Console.WriteLine("University Program Name: " + upProgramName);
            Console.WriteLine("Unviersity Degree Offered: " + upDegreeOffered);
            Console.WriteLine("University Department Head: " + upDepartmendHead);
            Console.ReadLine();
        }
    }
}