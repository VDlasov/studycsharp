﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment4
{
    class Program
    {
        struct Student
        {
            public string firstName;
            public string lastName;
            public DateTime dateOfBirth;
            public string adress1;
            public string adress2;
            public string cityName;
            public string state_province;
            public int zip_postal;
            public string country;
            public int studentId;
        };

        struct Teacher
        {
            public string firstName;
            public string lastName;
            public DateTime dateOfBirth;
            public string adress1;
            public string adress2;
            public string cityName;
            public string state_province;
            public int zip_postal;
            public string country;
            public int professorId;
        };

        struct UniversityProgram
        {
            public string name;
            public char degreeLevel; // 'm' for master 'b' for bachelor ...
            public string departmentHead;
        };

        struct Course
        {
            public string name;
            public string level;
            public string department;
        }

        static void printStudentDetails(Student temp)
        {
            Console.WriteLine("Student info: ");
            Console.WriteLine("{0} {1} {2} {3} {4} {5} {6} {7} {8} {9}", temp.firstName, temp.lastName, temp.dateOfBirth, temp.adress1, temp.adress2, temp.cityName, temp.state_province, temp.zip_postal, temp.country, temp.studentId);
        }

        static void Main(string[] args)
        {
            Student[] students = new Student[5];
            students[0].firstName = "MyFirstName";
            students[0].lastName = "MyLastName";
            students[0].adress1 = "My Adress";
            students[0].cityName = "MyCity";
            students[0].state_province = "MyState";
            students[0].zip_postal = 1911;
            students[0].country = "GR";
            students[0].studentId = 1234;
            printStudentDetails(students[0]);
        }
    }
}