﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Assignment5
{
    class Program
    {
        static void Main(string[] args)
        {
            // Instantiate 3 student objects
            Student student1 = new Student("Meryama", "Nadim", "3rd May 1990", "Belgium");
            Student student2 = new Student("Soukaina", "Legsabi", "24 December 1990", "Morocco");
            Student student3 = new Student("Omar", "Nadim", "30 June 1988", "France");

            // Instantiate a course object called "Programming with C#"
            Course myCourse = new Course("Programming with C#", 6);

            // Add 5 grades to the stack of each student
            for (int i = 0; i < 5; i++)
                student1.Grades.Push("A");
            for (int i = 0; i < 5; i++)
                student2.Grades.Push("B");
            for (int i = 0; i < 5; i++)
                student3.Grades.Push("C");

            // Add my three students to this course in the Students ArrayList
            myCourse.Students.Add(student1);
            myCourse.Students.Add(student2);
            myCourse.Students.Add(student3);

            // Call the method ListStudents from Course
            myCourse.ListStudents();

            // Instantiate 1 teacher object and add it to this course
            Teacher teacher1 = new Teacher("Mario", "Fornarolli");
            myCourse.Teachers[0] = teacher1;

            // Instantiate a degree object called Bachelor and add myCourse to it
            Degree bachelor = new Degree("Bachelor of science", 100);
            bachelor.Course = myCourse;

            // Instantiate a University Program called "Information Technology"
            UProgram myprogram = new UProgram("Information Technology", "Mario Fornarolli");
            myprogram.Degrees = bachelor;

            // output to console
            Console.WriteLine("The {0} program contains the {1} degree", myprogram.Name, myprogram.Degrees.Name);
            Console.WriteLine("The {0} degree contains the course {1}", myprogram.Degrees.Name, myprogram.Degrees.Course.Name);
            Console.WriteLine("The {0} course contains {1} student<s>", myprogram.Degrees.Course.Name, Student.count);

            Console.ReadLine();
        }
    }
}

namespace Assignment5
{
    class Course
    {
        #region Fields

        private string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        private int _credits;

        public int Credits
        {
            get { return _credits; }
            set { _credits = value; }
        }
        private Teacher[] _teachers;

        internal Teacher[] Teachers
        {
            get { return _teachers; }
            set { _teachers = value; }
        }
        private List<Student> _students;

        internal List<Student> Students
        {
            get { return _students; }
            set { _students = value; }
        }

        #endregion

        #region constructors

        public Course(string name, int credits)
        {
            this.Teachers = new Teacher[3];
            this.Students = new List<Student>();
            this.Name = name;
            this.Credits = credits;
        }
        #endregion

        #region methods

        public void ListStudents()
        {
            // Output the first and last name of each student
            int j = 0;
            foreach (Student student in Students)
            {
                Console.WriteLine("Student {0} : {1} {2}", j + 1, student.FirstName, student.LastName);
                j++;
            }
        }

        #endregion
    }
}

namespace Assignment5
{
    class Student : Person
    {
        #region Fields

        private string _birthday;

        public string Birthday
        {
            get { return _birthday; }
            set { _birthday = value; }
        }
        private string _country;

        public string Country
        {
            get { return _country; }
            set { _country = value; }
        }

        private Stack<string> _grades;

        public Stack<string> Grades
        {
            get { return _grades; }
            set { _grades = value; }
        }

        #endregion

        #region static variables

        public static int count = 0;

        #endregion

        #region constructors

        public Student(string firstName, string lastName, string birthday, string country)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Birthday = birthday;
            this.Country = country;
            this.Grades = new Stack<string>();
            count++;
        }
        #endregion

        #region methods

        public void TakeTest()
        {

        }

        #endregion
    }
}