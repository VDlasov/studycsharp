﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Mod_9_Homework_peer3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private int currStudentIndex = 0; //current student to display in the window

        //•Create a collection to store Student objects (Use a List<T> for this assignment)
        List<Student> student = new List<Student>();

        public MainWindow()
        {
            InitializeComponent();

            //Set the default cursor focus on the "First Name" field
            txtFirstName.Focus();

            //Turn the "<" and ">" button on/off, based on whether you are at the beginning, middle, or end of the list.
            EnableBtnNext();
            EnableBtnPrevious();
        }

        //Create new Student record
        //•Implement the code in the button click event handler to create a Student object and add it to the collection
        private void btnCreateStudent_Click(object sender, RoutedEventArgs e)
        {
            if (txtFirstName.Text != "" && txtLastName.Text != "")
            {
                try
                {
                    //Add the student to the collection
                    student.Add(new Student(firstName: txtFirstName.Text
                    , lastName: txtLastName.Text
                    , city: txtCity.Text));

                    //Set the student index to past the very end, to be ready to create the next record.
                    currStudentIndex = student.Count;

                    MessageBox.Show("Successfully created Student #" + student.Count);

                    //Turn the "<" and ">" button on/off, based on whether you are at the beginning, middle, or end of the list.
                    EnableBtnNext();
                    EnableBtnPrevious();

                    //•Clear the contents of the text boxes in the event handler 
                    ClearAllFields();
                }
                catch (Exception exception)
                {
                    MessageBox.Show("Error occurred while attempting to creat Student: " + exception.ToString());
                }
            }
            else
            {
                MessageBox.Show("Please enter the Student's name.");
                txtFirstName.Focus();
            }

        }

        //Show next Student record
        //•There are two additional buttons on the form that can be used to move through a collection of students, (previous (<) and next (>) ).
        //•Create event handlers for each of these buttons that will iterate over your Students collection and display the values in the text boxes
        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            //Increment the Index #
            currStudentIndex += 1;

            //Display the next student
            ShowStudentRec(currStudentIndex);

            //Turn the "<" and ">" button on/off, based on whether you are at the beginning, middle, or end of the list.
            EnableBtnNext();
            EnableBtnPrevious();
        }

        //Show previous Student record
        //•There are two additional buttons on the form that can be used to move through a collection of students, (previous (<) and next (>) ).
        //•Create event handlers for each of these buttons that will iterate over your Students collection and display the values in the text boxes
        private void btnPrevious_Click(object sender, RoutedEventArgs e)
        {
            //Decrement the Index #
            currStudentIndex -= 1;

            //Display the previous student
            ShowStudentRec(currStudentIndex);

            //Turn the "<" and ">" button on/off, based on whether you are at the beginning, middle, or end of the list.
            EnableBtnNext();
            EnableBtnPrevious();
        }

        //Clear all text fields on the form
        private void ClearAllFields()
        {
            txtFirstName.Clear();
            txtLastName.Clear();
            txtCity.Clear();

            //Set the cursor focus to the "First Name" field
            txtFirstName.Focus();
        }

        //Display the student record, for the given index number.
        private void ShowStudentRec(int index)
        {
            try
            {
                txtFirstName.Text = student[index].FirstName;
                txtLastName.Text = student[index].LastName;
                txtCity.Text = student[index].City;
            }
            catch (Exception exception)
            {
                ClearAllFields();
            }

            //Set the cursor focus to the "First Name" field
            txtFirstName.Focus();
        }

        //Turn the Next ">" button on/off, based on whether you are at the end of the list.
        //Challenge: Search on the properties of the buttons to learn how to disable the buttons if you are at the beginning or end of the collection.
        private void EnableBtnNext()
        {
            if (currStudentIndex > (student.Count - 1))
                btnNext.IsEnabled = false;
            else
                btnNext.IsEnabled = true;
        }

        //Turn the Preview "<" button on/off, based on whether you are at the beginning of the list.
        //Challenge: Search on the properties of the buttons to learn how to disable the buttons if you are at the beginning or end of the collection.
        private void EnableBtnPrevious()
        {
            if (currStudentIndex <= 0)
                btnPrevious.IsEnabled = false;
            else
                btnPrevious.IsEnabled = true;
        }

        //Save the Student when you hit ENTER from any text box.
        private void txtFirstName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                btnCreateStudent_Click(this, new RoutedEventArgs());
        }
    }

    //•Add a new class to the project to represent a Student with three properties for the text fields.
    class Student
    {
        private string firstName;
        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }

        private string lastName;
        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }

        private string city;
        public string City
        {
            get { return city; }
            set { city = value; }
        }

        //Constructor
        public Student(string firstName
        , string lastName
        , string city)
        {
            //3.Modify your Student and Teacher classes so that they inherit the common attributes from Person
            this.FirstName = firstName;
            this.LastName = lastName;
            this.City = city;
        }
    }
}