﻿using ModuleFive;
using System;
using System.Collections.Generic;

namespace ModuleFive
{
    public class Student
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Birthdate { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public static int CountStudents;

        public Student()
        {
            CountStudents++;
        }
    }
}


namespace ModuleFive
{
    public class Teacher
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Birthdate { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public int HoursToWork { get; set; }

        public Teacher()
        {
        }
    }
}


namespace ModuleFive
{
    public class Course
    {
        public string Name { get; set; }
        public List<Student> Students { get; set; }
        public List<Teacher> Teachers { get; set; }
        public Course(string name)
        {
            this.Name = name;
            this.Students = new List<Student>();
            this.Teachers = new List<Teacher>();
        }
    }
}


namespace ModuleFive
{
    public class Degree
    {
        public string Name { get; set; }
        public int Credits { get; set; }
        public List<Course> Courses { get; set; }

        public Degree(string name)
        {
            this.Name = name;
            Courses = new List<Course>();
        }
    }
}


namespace ModuleFive
{
    public class UProgram
    {
        public string Name { get; set; }
        public List<Degree> Degrees { get; set; }
        public string Department { get; set; }

        public UProgram(string name)
        {
            this.Name = name;
            Degrees = new List<Degree>();
        }
    }
}


namespace ModuleFile
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Student student1 = new Student();
            Student student2 = new Student();
            Student student3 = new Student();

            Course course = new Course("Programming with C#");
            course.Students.Add(student1);
            course.Students.Add(student2);
            course.Students.Add(student3);

            Teacher teacher1 = new Teacher();
            Teacher teacher2 = new Teacher();
            Teacher teacher3 = new Teacher();

            course.Teachers.Add(teacher1);
            course.Teachers.Add(teacher2);
            course.Teachers.Add(teacher3);

            Degree degree = new Degree("Bachelor");
            degree.Courses.Add(course);

            UProgram program = new UProgram("Information Technology");
            program.Degrees.Add(degree);

            Console.WriteLine("The {0} program contains the {1} degree", program.Name, degree.Name);
            Console.WriteLine("The {0} degree contains the course {1}", degree.Name, course.Name);
            Console.WriteLine("The {0} course contains {1} student(s)", course.Name, Student.CountStudents);
        }
    }
}