﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cSharp_Home7
{
    class Program
    {
        static void Main(string[] args)
        {
            var vRandom = new Random();
            var universityCourse = new Course("The Programming with C#");
            universityCourse.addStudent(new Student(1, "Albert", "Enstein", new DateTime(1879, 3, 14), "Ulm, Kingdom of Württemberg, German Empire"));
            universityCourse.addStudent(new Student(2, "Grigori", "Perelman", new DateTime(1966, 06, 13), "Saint Petersburg, Russia"));
            universityCourse.addStudent(new Student(3, "Alan", "Turing", new DateTime(1912, 06, 23), "Paddington, London"));

            universityCourse.addTeacher(new Teacher("Michael", "Faraday", "Professor"));
            universityCourse.addTeacher(new Teacher("Niels Henrik David", "Bohr", "Graduate"));

            foreach (Student vStudent in universityCourse.StudentsList)
            {
                for (int i = 0; i < 5; i++)
                {
                    vStudent.Grades.Push(vRandom.Next(0, 101));
                }
            }

            Degree vDegree = new Degree("Barchelor of Science");
            vDegree.addNewCourse(universityCourse);

            UProgram vProgram = new UProgram("The Information Technology");
            vProgram.addDegree(vDegree);

            foreach (Degree vProgramDegree in vProgram.DegreeName)
            {
                string vDeg = vProgramDegree.DegreeName;
                Console.WriteLine("{0} program contains {1} degree", vProgram.ProgramName, vDeg);
                Console.WriteLine();

                foreach (Course vCourse in vProgram.DegreeName[0].CoursesList)
                {
                    string vCourseName = vCourse.CourseName;
                    Console.WriteLine("{0} contains the course {1}", vDeg, vCourseName);
                    Console.WriteLine();
                    Console.WriteLine("{0} contains {1} teacher(s) and {2} student(s)", vCourseName, vCourse.TeachersList.Count, vCourse.StudentsList.Count);
                    Console.WriteLine();
                    Console.WriteLine("Teacher(s):");
                    foreach (Teacher vTeacher in vCourse.TeachersList)
                    {
                        Console.WriteLine("Teacher: {0} {1}", vTeacher.FirstName, vTeacher.LastName);
                        Console.WriteLine("Teacher Degree: {0}", vTeacher.TeacherDegree);
                        vTeacher.gradeTest(vTeacher.GradeTests);
                    }

                    Console.WriteLine();
                    Console.WriteLine("Student(s):");
                    foreach (Student vStudent in vCourse.StudentsList)
                    {
                        Console.WriteLine("{0}: {1} {2}", vStudent.ID, vStudent.FirstName, vStudent.LastName);
                        Console.WriteLine("Birth Date: {0}", vStudent.Birthdate.ToShortDateString());
                        Console.WriteLine("Address: {0}", vStudent.Address);
                        Console.WriteLine("Student {0} {1} final grade at the end of the {2} is {3}", vStudent.FirstName, vStudent.LastName, vCourseName, vStudent.Grades.Pop());
                        Console.WriteLine();
                    }
                }
            }

            Console.ReadLine();
        }
    }

    public class Person
    {
        private string _firstName;
        private string _lastName;
        private string _address;

        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

        public string Address
        {
            get { return _address; }
            set { _address = value; }
        }

        public Person(string pFirstName, string pLastName)
        {
            this.FirstName = pFirstName;
            this.LastName = pLastName;
        }

        public Person(string pFirstName, string pLastName, string pAddress)
        {
            this.FirstName = pFirstName;
            this.LastName = pLastName;
            this.Address = pAddress;
        }
    }

    public class Student : Person
    {
        private int _id = 0;
        private DateTime _birthdate;
        public Stack Grades = new Stack();

        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public DateTime Birthdate
        {
            get { return _birthdate; }
            set { _birthdate = value; }
        }

        public Student(int pID, string pFirstName, string pLastName, DateTime pBirthDate, string pAddress)
        : base(pFirstName, pLastName, pAddress)
        {
            this.ID = pID;
            this.Birthdate = pBirthDate;
        }

        void takeTest()
        {
            Console.WriteLine("");
        }
    }

    public class Teacher : Person
    {
        private string _teacherDegree;
        private bool _gradeTests;

        public string TeacherDegree
        {
            get { return _teacherDegree; }
            set { _teacherDegree = value; }
        }

        public bool GradeTests
        {
            get { return _gradeTests; }
            set { _gradeTests = value; }
        }

        public Teacher(string pFirstName, string pLastName, string pTeacherDegree, bool pIsGradeTests = false)
        : base(pFirstName, pLastName)
        {
            this.TeacherDegree = pTeacherDegree;
            this.GradeTests = pIsGradeTests;
        }

        public void gradeTest(bool pIsTakeTests)
        {
            Console.WriteLine((pIsTakeTests) ? "Grades tests" : "Does not grade tests");
        }
    }

    public class Course
    {
        private string _courseName;
        private List<Student> _studentsList;
        private List<Teacher> _teachersList;

        public string CourseName { get { return _courseName; } set { _courseName = value; } }
        public List<Student> StudentsList { get { return _studentsList; } set { _studentsList = value; } }
        public List<Teacher> TeachersList { get { return _teachersList; } set { _teachersList = value; } }

        public Course(string pCourseName)
        {
            this.CourseName = pCourseName;
            TeachersList = new List<Teacher>();
            StudentsList = new List<Student>();
        }

        public void addTeacher(Teacher pTeacher)
        {
            TeachersList.Add(pTeacher);
        }

        public void addStudent(Student pStudent)
        {
            StudentsList.Add(pStudent);
        }
    }

    public class Degree
    {
        private string _degreeName;
        public string DegreeName { get { return _degreeName; } set { _degreeName = value; } }

        private List<Course> _courseList;
        public List<Course> CoursesList { get { return _courseList; } set { _courseList = value; } }

        public Degree(string pDegreeName)
        {
            this.DegreeName = pDegreeName;
            CoursesList = new List<Course>();
        }

        public void addNewCourse(Course pCourse)
        {
            this.CoursesList.Add(pCourse);
        }
    }

    public class UProgram
    {
        private string _programName;
        public string ProgramName { get { return _programName; } set { _programName = value; } }

        private List<Degree> _degreeName;
        public List<Degree> DegreeName;

        public UProgram(string pProgramName)
        {
            this.ProgramName = pProgramName;
            this.DegreeName = new List<Degree>();
        }

        public void addDegree(Degree pDegree)
        {
            DegreeName.Add(pDegree);
        }
    }
}