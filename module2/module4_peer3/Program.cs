﻿namespace Assignment
{
    using System;

    class Program
    {
        static void Main(string[] args)
        {
            Student[] students = new Student[5];
            students[0] = new Student("John", "Doe", new DateTime(1993, 07, 17), "43 Guild Street, London, N6 9PL");
            Console.WriteLine("Student Info");
            Console.WriteLine("Name: {0} {1}", students[0].FirstName, students[0].LastName);
            Console.WriteLine("Date of Birth: {0}", students[0].BirthDate.ToShortDateString());
            Console.WriteLine("Address: {0}", students[0].Address);

            Console.ReadLine();
        }

        struct Student
        {
            public String FirstName;
            public String LastName;
            public DateTime BirthDate;
            public String Address;

            public Student(String firstName, String lastName, DateTime birthDate, String address)
            {
                this.FirstName = firstName;
                this.LastName = lastName;
                this.BirthDate = birthDate;
                this.Address = address;
            }
        }

        struct Teacher
        {
            public String FirstName;
            public String LastName;
            public DateTime BirthDate;
            public String Address;

            public Teacher(String firstName, String lastName, DateTime birthDate, String address)
            {
                this.FirstName = firstName;
                this.LastName = lastName;
                this.BirthDate = birthDate;
                this.Address = address;
            }
        }

        enum Degree
        {
            Bachelor, Foundation, Master, Doctorate
        }

        struct Programme
        {
            public String Name;
            public Degree[] Degrees;
            public Teacher Head;

            public Programme(String name, Degree[] degrees, Teacher head)
            {
                this.Name = name;
                this.Degrees = degrees;
                this.Head = head;
            }
        }

        struct Course
        {
            public String Name;
            public UInt16 Credits;

            public Course(String name, UInt16 credits)
            {
                this.Name = name;
                this.Credits = credits;
            }
        }
    }
}