﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace module3
{
    class Program
    {
        private static DateTime ReadDateTimeFromConsole(string message)
        {
            DateTime readedDate;
            bool isCorrectDate = false;
            do
            {
                Console.WriteLine(message);
                string dateInput = Console.ReadLine();
                isCorrectDate = DateTime.TryParse(s: dateInput, result: out readedDate);
            } while (!isCorrectDate);
            return readedDate;
        }

        private static string ReadDateStringConsole(string message)
        {
            string readedString = "Null";
            bool isCorrectString = false;
            do
            {
                Console.WriteLine(message);
                string dateInput = Console.ReadLine();
                if (!String.IsNullOrWhiteSpace(dateInput))
                {
                    isCorrectString = true;
                    readedString = dateInput;
                }
            } while (!isCorrectString);
            return readedString;
        }


        // Students: first name, last name, birthday, address
        static void PrintStudentDetails(string first, string last, DateTime birthday, string address)
        {
            Console.WriteLine("\nStudent: {0} {1} was born on: {2}\nAdress: {3}\n", first, last, birthday.ToShortDateString(), address);
            //throw new NotImplementedException();
        }

        static void GetStudentInfo()
        {
            string firstName = ReadDateStringConsole(message: "Enter the student's first name:");
            string lastName = ReadDateStringConsole(message: "Enter the student's last name:");
            DateTime birthday = ReadDateTimeFromConsole(message: "Enter the student's birthday:");
            string address = ReadDateStringConsole(message: "Enter the student's address:");
            
            PrintStudentDetails(firstName, lastName, birthday, address);
            //throw new NotImplementedException();
        }


        // Teachers: first name, last name, birthday, address
        static void PrintTeacherDetails(string first, string last, DateTime birthday, string address)
        {
            Console.WriteLine("\nTeacher: {0} {1} was born on: {2}\nAdress: {3}\n", first, last, birthday.ToShortDateString(), address);
            //throw new NotImplementedException();
        }

        static void GetTeacherInfo()
        {
            string firstName = ReadDateStringConsole(message: "Enter the teacher's first name:");
            string lastName = ReadDateStringConsole(message: "Enter the teacher's last name:");
            DateTime birthday = ReadDateTimeFromConsole(message: "Enter the teacher's birthday:");
            string address = ReadDateStringConsole(message: "Enter the teacher's address:");

            PrintTeacherDetails(firstName, lastName, birthday, address);       
            //throw new NotImplementedException();
        }


        // Courses: name, start date, end date, number of credit hours 
        static void PrintCourseDetails(string name, DateTime startDate, DateTime endDate, int numCreditHours)
        {
            Console.WriteLine("\nCourse: {0}. Credit hours: {1}.\nStart date {2} - {3} End date\n", name, numCreditHours, startDate.ToShortDateString(), endDate.ToShortDateString());
            //throw new NotImplementedException();
        }

        static void GetCourseInfo()
        {
            string name = ReadDateStringConsole(message: "Enter the course's name:");
            DateTime startDate = ReadDateTimeFromConsole(message: "Enter the course's start date:");
            DateTime endDate = ReadDateTimeFromConsole(message: "Enter the course's end date:");
            int numCreditHours = 0;
            int.TryParse(s: ReadDateStringConsole(message: "Enter the course's credit hours:"), result: out numCreditHours);

            PrintCourseDetails(name, startDate, endDate, numCreditHours);
            //throw new NotImplementedException();
        }


        // Program: subjects, exams, coursework - "Math, Algorithms, ..."
        static void PrintProgramDetails(string subjects, string exams, string coursework)
        {
            Console.WriteLine("\nSubjects: {0}.\nSubjects with exams: {1}.\nSubjects with coursework: {2}.\n", subjects, exams, coursework);
            //throw new NotImplementedException();
        }

        static void GetProgramInfo()
        {
            string subjects = ReadDateStringConsole(message: "Enter the program's subjects. Separete by \",\":");
            string exams = ReadDateStringConsole(message: "Enter the program's subjects that have exams. Separete by \",\":");
            string coursework = ReadDateStringConsole(message: "Enter the program's subjects that have courseworks. Separete by \",\":");

            PrintProgramDetails(subjects, exams, coursework);
            //throw new NotImplementedException();
        }


        // Name of university, like "MAI", create date: "01.01.1930" or another, education - Master, Bachelor, Ph.D.
        static void PrintDegreeDetails(string name, DateTime createDate, string education)
        {
            Console.WriteLine("\nThe {0} was created in {1}.\nHave next education: {2}\n", name, createDate.ToShortDateString(), education);
            //throw new NotImplementedException();
        }

        static void GetDegreeInfo()
        {
            string name = ReadDateStringConsole(message: "Enter the name of university:");
            DateTime createDate = ReadDateTimeFromConsole(message: "Enter the date university was created:");
            string education = ReadDateStringConsole(message: "Enter the education in iniversite. (Master, Bachelor, Ph.D.) Separete by \",\":");

            PrintDegreeDetails(name, createDate, education);
            //throw new NotImplementedException();
        }



        // Method for test NotImplementedException exeption
        static void ValidatingStudentBirthday()
        {
            string methodName = MethodBase.GetCurrentMethod().Name;
            throw new NotImplementedException(message: methodName);
        }

        static void Main(string[] args)
        {
            // Main part. Input from console, then print
            GetStudentInfo();
            GetTeacherInfo();
            GetCourseInfo();
            GetProgramInfo();
            GetDegreeInfo();

            try // Raise NotImplementedException exeption
            {
                ValidatingStudentBirthday(); 
            }
            catch (NotImplementedException ex) // Catch NotImplementedException exeption
            {
                Console.WriteLine("I handle exeption! The method: \"{0}\" is not implemented!", ex.Message);
            }

        }
    }
}
