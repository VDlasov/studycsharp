﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Mod_9_Homework
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Student> students = new List<Student>();
        int currentSelected = -1;

        #region property for students and current selected student
        public int CurrentSelected
        {
            get
            {
                return currentSelected;
            }

            set
            {
                currentSelected = value;
            }
        }

        public List<Student> Students
        {
            get
            {
                return students;
            }

            set
            {
                students = value;
            }
        }
        #endregion

        public MainWindow()
        {
            InitializeComponent();
        }

        // 1 Event handler created for the Create Student button
        private void btnCreateStudent_Click(object sender, RoutedEventArgs e)
        {
            // 2 Event handler creates a Student object using values from the text boxes on the form
            var newStudent = new Student() { LastName = txtFirstName.Text, FirstName = txtFirstName.Text, City = txtCity.Text };

            // 3 Textbox values are cleared
            txtFirstName.Clear();
            txtLastName.Clear();
            txtCity.Clear();

            // 4 Event handler adds a Student object to the List<T>
            Students.Add(newStudent);
        }

        // 5 Next button displays each student's information in the text boxes
        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            if (Students.Any())
            {
                if (CurrentSelected == -1)
                {
                    CurrentSelected = 0;
                }
                else
                {
                    CurrentSelected = (++CurrentSelected) % (Students.Count);
                }
                txtFirstName.Text = Students[CurrentSelected].FirstName;
                txtLastName.Text = Students[CurrentSelected].LastName;
                txtCity.Text = Students[CurrentSelected].City;
            }
        }

        // 6 Previous button displays each student's information in the text boxes
        private void btnPrevious_Click(object sender, RoutedEventArgs e)
        {
            if (Students.Any())
            {
                if (CurrentSelected == -1)
                {
                    CurrentSelected = 0;
                }
                else
                {
                    CurrentSelected--;
                    if (CurrentSelected < 0)
                        CurrentSelected = Students.Count - 1;
                }
                txtFirstName.Text = Students[CurrentSelected].FirstName;
                txtLastName.Text = Students[CurrentSelected].LastName;
                txtCity.Text = Students[CurrentSelected].City;
            }
        }
    }

    public class Student
    {
        private string firstName;
        private string lastName;
        private string city;

        #region Student properties
        public string FirstName
        {
            get
            {
                return firstName;
            }

            set
            {
                if (!String.IsNullOrWhiteSpace(value))
                    firstName = value;
                else
                    firstName = "Not provide";
            }
        }

        public string LastName
        {
            get
            {
                return lastName;
            }

            set
            {
                if (!String.IsNullOrWhiteSpace(value))
                    lastName = value;
                else
                    lastName = "Not provide";
            }
        }

        public string City
        {
            get
            {
                return city;
            }

            set
            {
                if (!String.IsNullOrWhiteSpace(value))
                    city = value;
                else
                    city = "Not provide";
            }
        }
        #endregion
    }
}
