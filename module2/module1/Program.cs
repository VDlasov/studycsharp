﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace module1 {
    class Program {
        static void Main(string[] args) {

            string FirstName = "Denis";
            string LastName = "Vlasov";
            string Birthdate = "05.02.1992";
            string AddressLine = "Nahimova st.";
            string City = "Khimki";
            string Postal = "872247";
            string Country = "Russia";
            string Phone = "+79997264727";
            string University = "MAI";
            string Degrees = "Master of Science in Information Technology";

            Console.WriteLine(FirstName);
            Console.WriteLine(LastName);
            Console.WriteLine(Birthdate);
            Console.WriteLine(AddressLine);
            Console.WriteLine(City);
            Console.WriteLine(Postal);
            Console.WriteLine(Country);
            Console.WriteLine(Phone);
            Console.WriteLine(University);
            Console.WriteLine(Degrees);
            Console.ReadKey();
        }
    }
}
