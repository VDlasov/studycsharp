﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Module3
{
    class Program
    {
        static void Main(string[] args)
        {
            GetStudentInfo();
            GetProfessorInfo();
            GetCourseInfo();
            GetProgramInfo();
            GetDegreeInfo();
            ValidateStudentsBDay();

        }// end main

        static void GetStudentInfo()
        {
            Console.WriteLine("Enter the student's first name: ");
            string firstName = Console.ReadLine();
            Console.WriteLine("Enter the student's last name: ");
            string lastName = Console.ReadLine();
            Console.WriteLine("Enter the student's birtday: ");
            string sBirthday = Console.ReadLine();

            PrintStudentDetails(firstName, lastName, sBirthday);
        }// end GetStudentInfo

        static void PrintStudentDetails(string first, string last, string birthday)
        {
            Console.WriteLine("{0} {1} was born on: {2}.", first, last, birthday);
        }// end PrintStudentDetails

        static void GetProfessorInfo()
        {
            Console.WriteLine("Enter the professor's first name: ");
            string pfirstName = Console.ReadLine();
            Console.WriteLine("Enter the professor's last name: ");
            string plastName = Console.ReadLine();
            Console.WriteLine("Enter the professor's rank(e.g. Assistant, Associate, Full): ");
            string pRank = Console.ReadLine();
            Console.WriteLine("Enter the department the professor's in: ");
            string pDepartment = Console.ReadLine();

            PrintProfessorDetails(pfirstName, plastName, pRank, pDepartment);
        }// end GetProfessorInfo

        static void PrintProfessorDetails(string pfirst, string plast, string rank, string department)
        {
            Console.WriteLine("{0} {1} is a/an {2} professor in the Department of {3}.", pfirst, plast, rank, department);
        }// end PrintProfessorDetails

        static void GetCourseInfo()
        {
            Console.WriteLine("Enter the course's title: ");
            string courseTitle = Console.ReadLine();
            Console.WriteLine("Enter the course's ID number: ");
            string courseID = Console.ReadLine();
            Console.WriteLine("Enter the course's number of credit hours: ");
            string courseCreditHours = Console.ReadLine();
            Console.WriteLine("Enter the department the course is offered by: ");
            string courseDepartment = Console.ReadLine();

            PrintCourseDetails(courseTitle, courseID, courseCreditHours, courseDepartment);
        }// end GetCourseInfo

        static void PrintCourseDetails(string title, string idnumber, string credits, string department)
        {
            Console.WriteLine("{0}, {1}, is a course worth {2} credit hours, offered by the Department of {3}.", title, idnumber, credits, department);
        }// end PrintCourseDetails

        static void GetProgramInfo()
        {
            Console.WriteLine("Enter the program's name: ");
            string progName = Console.ReadLine();
            Console.WriteLine("Enter the degree's offered under the program(Associate, Bachelor, Master, Doctorate): ");
            string degreeOffered = Console.ReadLine();
            Console.WriteLine("Enter the Department the program is offered by: ");
            string programDepart = Console.ReadLine();

            PrintProgramDetails(progName, degreeOffered, programDepart);

        }// end GetProgramInfo

        static void PrintProgramDetails(string prog, string degree, string depart)
        {
            Console.WriteLine("The {0} program offers a {1} degree under the Department of {2}.", prog, degree, depart);
        }// end PrintProgramDetails

        static void GetDegreeInfo()
        {
            Console.WriteLine("Enter the degree's designation(Associate, Bachelor, Master, Doctorate): ");
            string degreeDesig = Console.ReadLine();
            Console.WriteLine("Enter the degree's discipline(Mathematics, Physics, Computer Science, etc): ");
            string degreeDiscip = Console.ReadLine();
            Console.WriteLine("Enter the Department that offers the degree: ");
            string degreeDepart = Console.ReadLine();
            Console.WriteLine("Enter the number of credit hours one must successfully earn to obtain the degree: ");
            string numCredHours = Console.ReadLine();

            PrintDegreeDetails(degreeDesig, degreeDiscip, numCredHours);
        }// end GetDegreeInfo

        static void PrintDegreeDetails(string designation, string discipline, string num)
        {
            Console.WriteLine("The {0}'s in {1} requires {2} credit hours to complete.", designation, discipline, num);
        }// end PrintDegreeDetails

        static void ValidateStudentsBDay()
        {
            //didn't write this yet, per instructions
            throw new NotImplementedException();
        }
    }// end class
}// end namespace