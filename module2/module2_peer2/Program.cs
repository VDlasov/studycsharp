﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace module2_peer2
{
    class Program
    {
        static void Main(string[] args)
        {
            string even_position = "X"; // topleft is even (0 + 0)
            string odd_position = "O";
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    // By adding the rowNo to the ColNo and then taking the modulo,
                    // even rows will start with 'O' and odd rows with 'X'.
                    Console.Write((i + j) % 2 == 0 ? odd_position : even_position);
                }
                Console.WriteLine();
            }
            Console.ReadLine();
        }
    }
}
