﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstCSharpConsoleApplication
{
    public class Person
    {
        public string name;
        public char gender;
        public int dateOfBirth;
    }

    public class Student : Person
    {
        public Student()
        {
            classCount++;
        }

        static private int classCount = 0;

        public static int ClassCount
        {
            get
            {
                return Student.classCount;
            }
            set
            {
                classCount = value;
            }
        }

        public void TakeTest()
        {
            Console.WriteLine("a test is going to be taken!");
        }

    }

    public class Teacher : Person
    {
        public void GradeTest()
        {
            Console.WriteLine("a test is going to be issued!");
        }
    }

    public class UProgram
    {
        private Degree degree = new Degree();

        public Degree Degree
        {
            get { return degree; }
            set { degree = value; }
        }
        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
    }

    public class Degree
    {
        private Course course = new Course();

        public Course Course
        {
            get { return course; }
            set { course = value; }
        }

        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
    }

    public class Course
    {
        private Student[] stu = new Student[3];
        private Teacher teacher = new Teacher();

        public Teacher Teacher
        {
            get { return teacher; }
            set { teacher = value; }
        }

        public Student[] Stu
        {
            get { return stu; }
            set { stu = value; }
        }

        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
    }

    class program
    {
        static void Main(string[] args)
        {
            //1.Instantiate three Student objects.
            var John = new Student();

            var Bob = new Student();

            Student Lee = new Student();

            //2.Instantiate a Course object called Programming with C#.
            var ProgrammingWithCSharp = new Course();
            ProgrammingWithCSharp.Name = "Programming with C#";

            //3.Add your three students to this Course object.
            ProgrammingWithCSharp.Stu[0] = John;
            ProgrammingWithCSharp.Stu[1] = Bob;
            ProgrammingWithCSharp.Stu[2] = Lee;

            //4.Instantiate at least one Teacher object.
            var James = new Teacher();

            //5.Add that Teacher object to your Course object
            ProgrammingWithCSharp.Teacher = James;

            //6.Instantiate a Degree object, such as Bachelor.
            var bachelor = new Degree();
            bachelor.Name = "Bachelor";

            //7.Add your Course object to the Degree object.
            bachelor.Course = ProgrammingWithCSharp;

            //8.Instantiate a UProgram object called Information Technology.
            var InformationTechnology = new UProgram();
            InformationTechnology.Name = "Information Technology";

            //9.Add the Degree object to the UProgram object.
            InformationTechnology.Degree = bachelor;

            //10.Using Console.WriteLine statements, output the following information to the console window:
            Console.WriteLine("The {0} program contains the {1} degree", InformationTechnology.Name, bachelor.Name);
            Console.WriteLine("The {0} degree contain the course {1}", bachelor.Name, ProgrammingWithCSharp.Name);
            Console.WriteLine("The {0} course contain {1} students.", ProgrammingWithCSharp.Name, Student.ClassCount);
        }
    }
}